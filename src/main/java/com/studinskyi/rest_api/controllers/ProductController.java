package com.studinskyi.rest_api.controllers;

import com.studinskyi.rest_api.entity.Product;
import com.studinskyi.rest_api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
@RestController
@RequestMapping("/goods")
public class ProductController {
    @Autowired
    ProductRepository goods;

    @RequestMapping(method = RequestMethod.GET)
    public List<Product> getUsers() {
        List<Product> result = new ArrayList<>();
        goods.findAll().forEach(result::add);
        return result;
    }



}
