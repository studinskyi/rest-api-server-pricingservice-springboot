package com.studinskyi.rest_api.controllers;

import com.studinskyi.rest_api.entity.Person;
import com.studinskyi.rest_api.entity.Product;
import com.studinskyi.rest_api.repository.PersonRepository;
import com.studinskyi.rest_api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
//@RestController
@RequestMapping("/")
public class SiteController {
    @Autowired
    private PersonRepository users;

    @Autowired
    private ProductRepository products;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        //(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/index");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("usersCollection", users.findAll());
        return modelAndView;
        //return new ModelAndView("site/index", Collections.singletonMap("usersCollection", users.findAll()));
    }

    @RequestMapping(value = "/add_users_form", method = RequestMethod.GET)
    public ModelAndView getAddUsersForm(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/add-users");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("usersCollection", users.findAll());
        return modelAndView;
        //return new ModelAndView("site/add-users", Collections.singletonMap("usersCollection", users.findAll()));
    }

    @RequestMapping(value = "/add_users_form", method = RequestMethod.POST)
    public String createNewUser(@Valid @RequestParam String firstName, @Valid @RequestParam String lastName,
                                @Valid @RequestParam String password, @Valid @RequestParam String password_confirm) {
        //empty fields not allowed
        if (firstName.isEmpty() || lastName.isEmpty() || password.isEmpty())
            return "redirect:/add_users_form";
        //passwords should match
        if (!password.equals(password_confirm))
            return "redirect:/add_users_form";

        users.save(new Person(firstName, lastName, password));
        //return users.save(new Person(firstName, lastName, password));
        return "redirect:/add_users_form";
    }

    @RequestMapping(value = "/report_for_certain_product_form", method = RequestMethod.GET)
    public ModelAndView getReportForCertainProductForm(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/report-for-certain-product");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("productsCollection", products.findAll());
        return modelAndView;
        //return new ModelAndView("site/report-for-certain-product", Collections.singletonMap("productsCollection", products.findAll()));
    }

    @RequestMapping(value = "/report_for_certain_product_form", method = RequestMethod.POST)
    public ModelAndView showReportForCertainProductForm(@Valid @RequestParam String name, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();

        //empty fields not allowed
        if (name.isEmpty()) {
            modelAndView.setViewName("site/report-for-certain-product");
            modelAndView.addObject("server_context_path", request.getContextPath());
            modelAndView.addObject("productsCollection", products.findAll());
            return modelAndView;
            //return new ModelAndView("site/report-for-certain-product", Collections.singletonMap("productsCollection", products.findAll()));
        }

        modelAndView.setViewName("site/report-for-certain-product");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("productsCollection", products.findByName(name));
        return modelAndView;
        //return new ModelAndView("site/report-for-certain-product", Collections.singletonMap("productsCollection", products.findByName(name)));
    }

    // <p>Выберите дату: <input type="date" name="calendar">
    @RequestMapping(value = "/report_prices_products_on_date", method = RequestMethod.GET)
    public ModelAndView getReportPricesProductsOnDate(HttpServletRequest request) {
        // actual prices for all products for certain timestamp: (фактические цены на все продукты для определенной временной метки)
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/report-prices-products-on-date");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("productsCollection", products.findAll());
        return modelAndView;
        //return new ModelAndView("site/report-prices-products-on-date", Collections.singletonMap("productsCollection", products.findAll()));
    }

    @RequestMapping(value = "/report_prices_products_on_date", method = RequestMethod.POST)
    public ModelAndView showReportPricesProductsOnDate(@RequestParam String dateOfPrice, HttpServletRequest request) {
        //        //empty fields not allowed
        //            return new ModelAndView(
        //        if (dateOfPrice.)"site/report-for-certain-product", Collections.singletonMap("productsCollection", products.findAll()));

        //        // for value '2016-01-05'; nested exception is java.lang.IllegalArgumentException
        //        String SQL_FORMAT_DATE = "yyyy-MM-dd";
        //        SimpleDateFormat formatter = new SimpleDateFormat(SQL_FORMAT_DATE, Locale.ROOT);
        //        Date utilDate = null;
        //        try {
        //            utilDate = formatter.parse(DateOfPrice);  //получаем распарсеную дату
        //        } catch (ParseException e) {
        //            //e.printStackTrace();
        //        }
        //
        //        java.sql.Date timestamp = new java.sql.Date(utilDate.getTime());

        //        String strDateOfPrice = "";
        //        try {
        //            strDateOfPrice = dateOfPrice.toString();
        //        } catch (Exception e) {
        //            //e.printStackTrace();
        //            return new ModelAndView("site/report-prices-products-on-date", Collections.singletonMap("productsCollection", products.findAll()));
        //        }
        //        if(strDateOfPrice.equals("")) {
        //            //return(null);
        //            return new ModelAndView("site/report-prices-products-on-date", Collections.singletonMap("productsCollection", products.findAll()));
        //        }

        ModelAndView modelAndView = new ModelAndView();
        // получение последней секунды обрабатываемого дня
        dateOfPrice = dateOfPrice + " 23:59:59";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ROOT);
        //        String SQL_FORMAT_DATE = "yyyy-MM-dd";
        //        SimpleDateFormat formatter = new SimpleDateFormat(SQL_FORMAT_DATE, Locale.ROOT);
        Date priceDate = null;
        try {
            priceDate = formatter.parse(dateOfPrice);  //получаем распарсеную дату
        } catch (ParseException e) {
            //e.printStackTrace();
            modelAndView.setViewName("site/report-prices-products-on-date");
            modelAndView.addObject("server_context_path", request.getContextPath());
            modelAndView.addObject("productsCollection", products.findAll());
            return modelAndView;
            //return new ModelAndView("site/report-prices-products-on-date", Collections.singletonMap("productsCollection", products.findAll()));
        }

        //        Calendar calendarUTC = Calendar.getInstance();
        //        int utcOffset = calendarUTC.get(Calendar.ZONE_OFFSET) + calendarUTC.get(Calendar.DST_OFFSET);
        //        long tempDate = priceDate.getTime();
        //        Date dateInUTC = new Date(tempDate - utcOffset);

        java.sql.Date dat_sql = null;
        try {
            dat_sql = new java.sql.Date(priceDate.getTime());
            //dat_sql = new java.sql.Date(priceDate.getTime());
        } catch (Exception e) {
            //e.printStackTrace();
            modelAndView.setViewName("site/report-prices-products-on-date");
            modelAndView.addObject("server_context_path", request.getContextPath());
            modelAndView.addObject("productsCollection", products.findAll());
            return modelAndView;
            //return new ModelAndView("site/report-prices-products-on-date", Collections.singletonMap("productsCollection", products.findAll()));
        }

        modelAndView.setViewName("site/report-prices-products-on-date");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("productsCollection", products.getPricesByDate(dat_sql));
        return modelAndView;
        //return new ModelAndView("site/report-prices-products-on-date", Collections.singletonMap("productsCollection", products.getPricesByDate(dat_sql)));
    }

    @RequestMapping(value = "/add_products_form", method = RequestMethod.GET)
    public ModelAndView getAddProductsForm(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/add-products");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("productsCollection", products.findAll());
        return modelAndView;
        //return new ModelAndView("site/add-products", Collections.singletonMap("productsCollection", products.findAll()));
    }

    @RequestMapping(value = "/add_products_form", method = RequestMethod.POST)
    public String createNewProduct(@Valid @RequestParam String name, @Valid @RequestParam int price) throws ParseException {
        //empty fields not allowed
        if (name.isEmpty() || price <= 0)
            return "redirect:/add_products_form";

        //	String startDate="01-02-2013";
        //	SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        //	java.util.Date date = sdf1.parse(startDate);
        //	java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
        //	timestamp DATETIME; NOT NULL;,
        //	price INT(11){
        //} NOT NULL;,

        //        SimpleDateFormat sdf = new SimpleDateFormat();
        //        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        //        Date dateInUtc = sdf.parse(myOriginalDate);

        //        String currentDateTime = new Date().toString();
        //        SimpleDateFormat sdf = new SimpleDateFormat();
        //        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        //        java.util.Date date = sdf.parse(currentDateTime);
        //        java.sql.Date sqlCurrentDateTime = new java.sql.Date(date.getTime());

                //Date dateField = Calendar.getInstance(TimeZone.getTimeZone("GMT+3:00")).getTime();
        //        Date currentTime = new Date();
        //        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm:ss a z");
        //        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        //        System.out.println("UTC time: " + sdf.format(currentTime));
                //        Calendar calendarUTC =
                //        Date dateInUTC =


        //        // вариант
        //        Calendar calendarUTC = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        //        Date dateInUTC = calendarUTC.getTime();
        //        // вариант
        //        Date dateInUTC = Instant.now();
        // вариант
        Calendar calendarUTC = Calendar.getInstance();
        int utcOffset = calendarUTC.get(Calendar.ZONE_OFFSET) + calendarUTC.get(Calendar.DST_OFFSET);
        long tempDate = new Date().getTime();
        Date dateInUTC = new Date(tempDate - utcOffset);

        products.save(new Product(name, dateInUTC, price));
        //products.save(new Product(name,new Date(),price));
        return "redirect:/add_products_form";

        //        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+01:00")); // Московское время
        //        System.out.println("Day: " + calendar.getTime());
        //
        //        calendar.add(Calendar.DAY_OF_MONTH, -1);
        //        System.out.println("Day: " + calendar.getTime());
        //
        //        // Пример перехода
        //
        //        calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+01:00")); // Московское время
        //        calendar.set(2006,8,01); // 1 Сентября 2006 года (не забываем, что месяц в Календаре начинается с "0")
        //        System.out.println("Day: " + calendar.getTime());
        //
        //        calendar.add(Calendar.DAY_OF_MONTH, -1);
        //        System.out.println("Day: " + calendar.getTime());
    }

    @RequestMapping(value = "/report_users_form", method = RequestMethod.GET)
    public ModelAndView getReportUsersForm(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/report-users");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("usersCollection", users.findAll());
        return modelAndView;
        //return new ModelAndView("site/report-users", Collections.singletonMap("usersCollection", users.findAll()));
    }

    @RequestMapping(value = "/report_products_form", method = RequestMethod.GET)
    public ModelAndView getReportProductsForm(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("site/report-products");
        modelAndView.addObject("server_context_path", request.getContextPath());
        modelAndView.addObject("productsCollection", products.findAll());
        return modelAndView;
        //return new ModelAndView("site/report-products", Collections.singletonMap("productsCollection", products.findAll()));
    }

    //    public static String getCurrenDateTimeString() {
    //        Date d = new Date();
    //        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    //        return formatDate.format(d);
    //    }
    //
    //    public static String getCurrenDateTimeToNameFile() {
    //        Date d = new Date();
    //        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
    //        return formatDate.format(d);
    //    }

}
