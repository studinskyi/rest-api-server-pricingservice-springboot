package com.studinskyi.rest_api.controllers;

import com.studinskyi.rest_api.entity.Person;
import com.studinskyi.rest_api.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RestController
@RequestMapping("/users")
public class PersonController {
    @Autowired
    PersonRepository users;

    @RequestMapping(method = RequestMethod.GET)
    public List<Person> getUsers() {
        List<Person> result = new ArrayList<>();
        users.findAll().forEach(result::add);
        return result;
    }

    //    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    //    public ModelAndView getReportUsers(Model model) {
    //        //        List<Person> resultUsers = new ArrayList<>();
    //        //        users.findAll().forEach(resultUsers::add);
    //
    //        //model.addAttribute("resultUsers", "show list of users");
    //        //return new ModelAndView("reports/report_users", Collections.singletonMap("collectionUsers", users.findAll()));
    //        //return new ModelAndView("reports/report_users", Collections.singletonMap("collectionUsers", users.findAll()));
    //        //model.addAttribute("collectionusers", "show list of users");
    //        //model.addAttribute("collectionusers",Collections.singletonMap("collectionUsers", users.findAll()));
    //        return new ModelAndView("listUsers");
    //        //return new ModelAndView("listUsers",Collections.singletonMap("collectionUsers", users.findAll()));
    //    }

    //    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    //    public String registration(Model model) {
    //        model.addAttribute("userForm", new UserEntity());
    //
    //        return "registration";
    //    }

    //    @RequestMapping(value = "/reportusers", method = RequestMethod.GET)
    //    public String reportUsers(Model model) {
    //        return "reportusers";
    //       //return new ModelAndView("reportUsers");
    //        //return new ModelAndView("reportUsers", Collections.singletonMap("collectionUsers", users.findAll()));
    //    }

//    @RequestMapping(value = "/reportUsers", method = RequestMethod.GET)
//    public ModelAndView reportUsers(Model model) {
//        model.addAttribute("collectionUsers", users.findAll());
//        return new ModelAndView("reportUsers");
//        //return "reportUsers";
//        //return new ModelAndView("reportUsers", Collections.singletonMap("collectionUsers", users.findAll()));
//        //return new ModelAndView("reports/report_users");
//        //return new ModelAndView("reports/report_users", Collections.singletonMap("collectionUsers", users.findAll()));
//    }

    //    @RequestMapping
    //    //@RequestMapping(value = "/reportUsers")
    //    public ModelAndView reportUsers() {
    //        return new ModelAndView("reportUsers", Collections.singletonMap("collectionUsers", users.findAll()));
    //        //return new ModelAndView("reports/report_users");
    //        //return new ModelAndView("reports/report_users", Collections.singletonMap("collectionUsers", users.findAll()));
    //    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getUserForm() {
        return new ModelAndView("addUser");
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Person addUser(@Valid String firstName, @Valid String lastName,
                          @Valid String password, @Valid String password_confirm) {
        //empty fields not allowed
        if (firstName.isEmpty() || lastName.isEmpty() || password.isEmpty())
            return null;
        //passwords should match
        if (!password.equals(password_confirm))
            return null;

        //        users.save(new Person(firstName, lastName, password));
        //        return "redirect:/add_users_form";
        return users.save(new Person(firstName, lastName, password));

    }

    @RequestMapping(value = "/put/{id}", method = RequestMethod.POST)
    //@RequestMapping(value = "/put/{id}", method = RequestMethod.POST)
    public Person putUser(@PathVariable("id") Long id, @Valid String firstName, @Valid String lastName, @Valid String password) {
        //public Person putUser(@PathVariable("id") Long id, @RequestParam String firstName, @RequestParam String lastName, @RequestParam String password) {
        //(@RequestParam(value="name", required=false, defaultValue="World") String name)

        //empty fields not allowed
        if (firstName.isEmpty() || lastName.isEmpty() || password.isEmpty())
            return null;

        Person person_for_put = users.findOne(id);
        if (!person_for_put.equals(null)) {
            person_for_put.setFirstName(firstName);
            person_for_put.setLastName(lastName);
            person_for_put.setPassword(password);
            users.save(person_for_put);
        }
        //return users.save(new Person(firstName, lastName, password));
        return person_for_put;
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public void delete(@PathVariable("id") Long id) {
        users.delete(id);
        //return "redirect:/report_users_form";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Person getUser(@PathVariable("id") Long id) {
        return users.findOne(id);
    }


    //    private final AppointmentBook appointmentBook;
    //
    //    @Autowired
    //    public AppointmentsController(AppointmentBook appointmentBook) {
    //        this.appointmentBook = appointmentBook;
    //    }
    //
    //    @RequestMapping(method = RequestMethod.GET)
    //    public Map<String, Appointment> get() {
    //        return appointmentBook.getAppointmentsForToday();
    //    }
    //
    //    @RequestMapping(value="/{day}", method = RequestMethod.GET)
    //    public Map<String, Appointment> getForDay(@PathVariable @DateTimeFormat(iso=ISO.DATE) Date day, Model model) {
    //        return appointmentBook.getAppointmentsForDay(day);
    //    }
    //
    //    @RequestMapping(value="/new", method = RequestMethod.GET)
    //    public AppointmentForm getNewForm() {
    //        return new AppointmentForm();
    //    }
    //
    //    @RequestMapping(method = RequestMethod.POST)
    //    public String add(@Valid AppointmentForm appointment, BindingResult result) {
    //        if (result.hasErrors()) {
    //            return "appointments/new";
    //        }
    //        appointmentBook.addAppointment(appointment);
    //        return "redirect:/appointments";
    //    }
}
