package com.studinskyi.rest_api.repository;

import com.studinskyi.rest_api.entity.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.sql.Date;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "product", path = "product")
public interface ProductRepository extends CrudRepository<Product, Long> {
    //extends JpaRepository
    //extends PagingAndSortingRepository
    //extends CrudRepository

    //@Query("select p from Product p where p.name = :name")
    List<Product> findByName(@Param("name") String name);


    //@Query("select p from Product p where p.timestamp < ?1")
    @Query(value = "SELECT * FROM Product WHERE Product.timestamp = (select MAX(prod2.timestamp) from Product as prod2 where prod2.name=Product.name AND prod2.timestamp <= ?1) order by Product.price", nativeQuery = true)
    //@Query(value = "SELECT * FROM Product WHERE Product.timestamp = (select MAX(prod2.timestamp) from Product as prod2 where prod2.name=Product.name) order by Product.price", nativeQuery = true)
    //@Query(value = "SELECT * FROM Product p WHERE p.timestamp <= ?1", nativeQuery = true)
    //@Query("select p from Product p where p.timestamp < ?1") // запрос работает
    List<Product> getPricesByDate(Date timestamp);
    //List<Product> getPricesByDate(@Param("timestamp") Date timestamp);

    //	@Query("select p from Product p where p.attributes[:attribute] = :value")
    //	List<Product> findByAttributeAndValue(@Param("attribute") String attribute, @Param("value") String value);
}

//Example 47. Declare query at the query method using @Query
//public interface UserRepository extends JpaRepository<User, Long> {
//
//    @Query("select u from User u where u.emailAddress = ?1")
//    User findByEmailAddress(String emailAddress);
//}

//Example 48. Advanced like-expressions in @Query
//public interface UserRepository extends JpaRepository<User, Long> {
//
//    @Query("select u from User u where u.firstname like %?1")
//    List<User> findByFirstnameEndsWith(String firstname);
//}

//    Native queries
//    The @Query annotation allows to execute native queries by setting the nativeQuery flag to true.
//
//        Example 49. Declare a native query at the query method using @Query
//public interface UserRepository extends JpaRepository<User, Long> {
//
//    @Query(value = "SELECT * FROM USERS WHERE EMAIL_ADDRESS = ?1", nativeQuery = true)
//    User findByEmailAddress(String emailAddress);
//}
