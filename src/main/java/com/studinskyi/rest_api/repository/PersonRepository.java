package com.studinskyi.rest_api.repository;

import com.studinskyi.rest_api.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "person", path = "person")
public interface PersonRepository extends CrudRepository<Person, Long> {
    //extends JpaRepository
    //extends PagingAndSortingRepository
    //extends CrudRepository

    List<Person> findByFirstName(@Param("value") String name);
    List<Person> findByLastName(@Param("value") String name);
    //@Query("select p from Person p where p.last_name like :value")
    List<Person> findByLastNameContaining(@Param("value") String name);
}

//Example 47. Declare query at the query method using @Query
//public interface UserRepository extends JpaRepository<User, Long> {
//
//    @Query("select u from User u where u.emailAddress = ?1")
//    User findByEmailAddress(String emailAddress);
//}

//Example 48. Advanced like-expressions in @Query
//public interface UserRepository extends JpaRepository<User, Long> {
//
//    @Query("select u from User u where u.firstname like %?1")
//    List<User> findByFirstnameEndsWith(String firstname);
//}

//    Native queries
//    The @Query annotation allows to execute native queries by setting the nativeQuery flag to true.
//
//        Example 49. Declare a native query at the query method using @Query
//public interface UserRepository extends JpaRepository<User, Long> {
//
//    @Query(value = "SELECT * FROM USERS WHERE EMAIL_ADDRESS = ?1", nativeQuery = true)
//    User findByEmailAddress(String emailAddress);
//}

