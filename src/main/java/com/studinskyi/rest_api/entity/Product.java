package com.studinskyi.rest_api.entity;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.Assert;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "Product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    //@NotEmpty
    @Size(min = 2, max = 50)
    private String name;

    @Column(name = "timestamp", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date timestamp;
    //@Temporal(javax.persistence.TemporalType.TIMESTAMP)

    //	String startDate="01-02-2013";
    //	SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
    //	java.util.Date date = sdf1.parse(startDate);
    //	java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
    //	timestamp DATETIME; NOT NULL;,
    //	price INT(11){
    //} NOT NULL;,


    @Column(name = "price", nullable = false)
    @Min(1)
    private int price;
    //	private BigDecimal price;


    public Product(String name, Date timestamp, int price) {
        Assert.hasText(name, "Name must not be null or empty!");
        Assert.isTrue(price > 0, "Price must be greater than zero!");
        this.name = name;
        this.timestamp = timestamp;
        this.price = price;
    }

    protected Product() {
        // empty constructor
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getPrice() {
        return price;
    }
}
