package com.studinskyi.rest_api;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@ComponentScan
@Configuration
@EnableJpaRepositories
@Import(RepositoryRestMvcConfiguration.class)
@EnableAutoConfiguration
@SpringBootApplication
//@RestController
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {

//        // read application properties to class AppProperties
//        AppProperties appProp = new AppProperties();

        SpringApplication.run(Application.class, args);
    }

    @RequestMapping("/task")
    public String showTask() {
        return "test-task";
    }

    //    @RequestMapping("/")
    //    public String startApp() {
    //        return "test-task";
    //        //return "Service to display the prices of products";
    //    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    //    @SpringBootApplication
    //    public class GuestBook extends SpringBootServletInitializer {
    //
    //        public static void main(String[] args) {
    //            SpringApplication.run(GuestBook.class, args)
    //                    .registerShutdownHook();
    //        }
    //
    //        @Override
    //        protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    //            return builder.sources(GuestBook.class);
    //        }
    //    }
}


//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//@ComponentScan
//@Configuration
//@EnableJpaRepositories
//@Import(RepositoryRestMvcConfiguration.class)
//@EnableAutoConfiguration
//@SpringBootApplication
//public class Application {
//
//    public static void main(String[] args) {
//        SpringApplication.run(Application.class, args);
//    }
//
//    @RequestMapping("/task")
//    public String showTask() {
//        return "test-task";
//    }
//
//    //    @SpringBootApplication
//    //    public class GuestBook extends SpringBootServletInitializer {
//    //
//    //        public static void main(String[] args) {
//    //            SpringApplication.run(GuestBook.class, args)
//    //                    .registerShutdownHook();
//    //        }
//    //
//    //        @Override
//    //        protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//    //            return builder.sources(GuestBook.class);
//    //        }
//    //    }
//}

