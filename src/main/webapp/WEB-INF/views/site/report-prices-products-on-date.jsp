<%--<?xml version="1.0" encoding="UTF-8"?>--%>
<%--<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" version="2.0">--%>

<%--<jsp:directive.page contentType="text/html" pageEncoding="UTF-8"/>--%>
<%--<jsp:output doctype-root-element="html" doctype-system="about:legacy-compat" omit-xml-declaration="true"/>--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Report prices of products on the date</title>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/WEB-INF/resources/css/jquery.datetimepicker.css"/>
    <!--<link rel="stylesheet" type="text/css" href="/resources/css/jquery.datetimepicker.css"/>-->
    <!--<link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>-->

    <style type="text/css">
        TABLE {
            width: 300px; /* Ширина таблицы */
            border: 1px solid black; /* Рамка вокруг таблицы */
            background: cornsilk; /* Цвет фона таблицы */
        }

        TD, TH {
            text-align: center; /* Выравнивание по центру */
            padding: 3px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Рамка вокруг таблицы */
        }

        TH {
            background: #90A5B6; /* Цвет фона */
            color: white; /* Цвет текста */
            border-bottom: 2px solid black; /* Линия снизу */
        }

        .lc {
            font-weight: bold; /* Жирное начертание текста */
            text-align: left; /* Выравнивание по левому краю */
        }
    </style>

    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
    <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
    <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
    <!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
    <!--<script>-->
    <!--$( function() {-->
    <!--$( "#datepicker" ).datepicker({-->
    <!--showOn: "button",-->
    <!--buttonImage: "images/calendar.gif",-->
    <!--buttonImageOnly: true,-->
    <!--buttonText: "Select date"-->
    <!--});-->
    <!--} );-->
    <!--</script>-->

    <script type="text/javascript" src="${pageContext.request.contextPath}/WEB-INF/resources/js\jquery-3.1.1.min.js">)</script>
    <%--<script> alert("jquery-3.1.1.min.js подключен"); </script>--%>

    <script type="text/javascript" src="${pageContext.request.contextPath}/WEB-INF/resources/js/jquery.js"></script>
    <%--<script> alert("jquery.js подключен"); </script>--%>

    <%--<script type="text/javascript"--%>
                       <%--src="${pageContext.request.contextPath}/WEB-INF/resources/js/jquery.datetimepicker.full.js"></script>--%>
    <%--<script>--%>
        <%--//    $.datetimepicker.setLocale('en');--%>
        <%--//       alert("jjquery.datetimepicker.full.js подключен");--%>

        <%--$('#datepicker').datepicker({--%>
            <%--//        dayOfWeekStart : 1,--%>
            <%--//        lang:'en',--%>
            <%--//        disabledDates:['1986/01/08','1986/01/09','1986/01/10'],--%>
            <%--//        startDate:	'1986/01/05'--%>
        <%--});--%>
    <%--</script>--%>


    <!--<script type="text/javascript" src="D:\Java_job\pricing_service_api\src\main\webapp\resources\js\jquery-3.1.1.min.js">alert("jquery-3.1.1.min.js подключен")</script>-->


</head>
<body>

<h2>Report prices of products on the date</h2>
<br/>
<a href="${pageContext.request.contextPath}/">Return to start page</a>
<br/>
<a href="${pageContext.request.contextPath}/report_products_form">Report products prices</a>
<br/>
<hr/>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/WEB-INF/resources/js/jquery.datetimepicker.full.js"></script>
<script>
    //    $.datetimepicker.setLocale('en');
    //       alert("jjquery.datetimepicker.full.js подключен");

    $('#datepicker').datepicker({
        //        dayOfWeekStart : 1,
        //        lang:'en',
        //        disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
        //        startDate:	'1986/01/05'
    });
</script>

<form method="POST">
    <%--<input type="date" />--%>
    <%--<div><input type="text" id="datetimepicker2"/></div>--%>
    <%--<input id="datepicker" name="dateOfPrice" value="2017-03-15" type="text"/>--%>
    <!--<div><label> View the price on the date: <input id="calendar" name="dateOfPrice" value="" type="text"/> </label></div>-->
    <div><label> View the price on the date: <input type="date" name="dateOfPrice"/> </label></div>
    <br/>
    <!--<div><label> Price of product : <input type="number" name="price"/> </label></div>-->
    <!--<br/>-->
    <div><input type="submit" value="Refresh report"/></div>
</form>
<hr/>

<h3>Products prices</h3>
<table cellspacing="0">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Time stamp</th>
        <th>Price</th>
    </tr>
    <c:forEach items="${productsCollection}" var="productElement">
        <tr>
            <td><c:out value="${productElement.id}"/></td>
            <td class="lc"><c:out value="${productElement.name}"/></td>
            <td><c:out value="${productElement.timestamp}"/></td>
            <td><c:out value="${productElement.price}"/></td>
        </tr>
    </c:forEach>
</table>

</body>

</html>
<%--</jsp:root>--%>