<%--<?xml version="1.0" encoding="UTF-8"?>--%>
<%--<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" version="2.0">--%>

<%--<jsp:directive.page contentType="text/html" pageEncoding="UTF-8"/>--%>
<%--<jsp:output doctype-root-element="html" doctype-system="about:legacy-compat" omit-xml-declaration="true"/>--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Report for certain product</title>

    <style type="text/css">
        TABLE {
            width: 300px; /* Ширина таблицы */
            border: 1px solid black; /* Рамка вокруг таблицы */
            background: cornsilk; /* Цвет фона таблицы */
        }

        TD, TH {
            text-align: center; /* Выравнивание по центру */
            padding: 3px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Рамка вокруг таблицы */
        }

        TH {
            background: #90A5B6; /* Цвет фона */
            color: white; /* Цвет текста */
            border-bottom: 2px solid black; /* Линия снизу */
        }

        .lc {
            font-weight: bold; /* Жирное начертание текста */
            text-align: left; /* Выравнивание по левому краю */
        }
    </style>
</head>
<body>

<h2>Report for certain product</h2>
<br/>
<a href="${pageContext.request.contextPath}/">Return to start page</a>
<br/>
<a href="${pageContext.request.contextPath}/report_products_form">Report products prices</a>
<br/>
<hr/>

<form method="POST">

    <div><label> Name product: <input type="text" name="name"/> </label></div>
    <br/>
    <div><input type="submit" value="Refresh report"/></div>
</form>
<hr/>

<h3>Products prices</h3>
<table cellspacing="0">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Time stamp</th>
        <th>Price</th>
    </tr>
    <c:forEach items="${productsCollection}" var="productElement">
        <tr>
            <td><c:out value="${productElement.id}"/></td>
            <td class="lc"><c:out value="${productElement.name}"/></td>
            <td><c:out value="${productElement.timestamp}"/></td>
            <td><c:out value="${productElement.price}"/></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
<%--</jsp:root>--%>