<%--<?xml version="1.0" encoding="UTF-8"?>--%>
<%--<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:c="http://java.sun.com/jsp/jstl/core" version="2.0">--%>

<%--<jsp:directive.page contentType="text/html" pageEncoding="UTF-8"/>--%>
<%--<jsp:output doctype-root-element="html" doctype-system="about:legacy-compat" omit-xml-declaration="true"/>--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add users (list of customers)</title>

    <style type="text/css">
        TABLE {
            width: 300px; /* Ширина таблицы */
            border: 1px solid black; /* Рамка вокруг таблицы */
            background: cornsilk; /* Цвет фона таблицы */
        }

        TD, TH {
            text-align: center; /* Выравнивание по центру */
            padding: 3px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Рамка вокруг таблицы */
        }

        TH {
            background: #90A5B6; /* Цвет фона */
            color: white; /* Цвет текста */
            border-bottom: 2px solid black; /* Линия снизу */
        }

        .lc {
            font-weight: bold; /* Жирное начертание текста */
            text-align: left; /* Выравнивание по левому краю */
        }
    </style>
</head>
<body>

<h2>Add users (list of customers)</h2>
<a href="${pageContext.request.contextPath}/">Return to start page</a>
<br/>
<a href="${pageContext.request.contextPath}/report_users_form">Report list of users</a>
<br/>
<hr/>
<form method="POST">

    <div><label> First Name : <input type="text" name="firstName"/> </label></div>
    <br/>
    <div><label> Last Name : <input type="text" name="lastName"/> </label></div>
    <br/>
    <div><label> Password: <input type="password" name="password"/> </label></div>
    <br/>
    <div><label> Password confirm: <input type="password" name="password_confirm"/> </label></div>
    <br/>
    <div><input type="submit" value="Add user"/></div>
    <!--<button type="submit">Submit</button>-->
    <br/>
</form>
<hr/>

<h3>Customers</h3>
<table cellspacing="0">
    <tr>
        <th>First name</th>
        <th>Last name</th>
    </tr>
    <c:forEach items="${usersCollection}" var="userElement">
        <tr>
            <td class="lc"><c:out value="${userElement.firstName}"/></td>
            <td><c:out value="${userElement.lastName}"/></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
<%--</jsp:root>--%>