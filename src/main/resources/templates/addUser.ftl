<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <title>Add user page</title>

    <link rel="stylesheet" type="text/css" href="/src/main/webapp/WEB-INF/resources/css/jquery.datetimepicker.css"/>
    <#--<link rel="stylesheet" type="text/css" href="D:\Java_job\pricing_service_api\src\main\webapp\resources\css\cal.css" />-->
    <script type="text/javascript" src="/src/main/webapp/WEB-INF/resources/js/jquery.js"></script>
    <script>
        alert("jquery.js подключен");
    </script>
    <script type="text/javascript" src="/src/main/webapp/WEB-INF/resources/js/jquery.datetimepicker.full.js"></script>
    <script>
        //    $.datetimepicker.setLocale('en');
        alert("jjquery.datetimepicker.full.js подключен");

        $('#datepicker').datepicker({
            //        dayOfWeekStart : 1,
            //        lang:'en',
            //        disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
            //        startDate:	'1986/01/05'
        });
    </script>
    <!--<script type="text/javascript" src="D:\Java_job\pricing_service_api\src\main\webapp\resources\js\jquery-3.1.1.min.js">alert("jquery-3.1.1.min.js подключен")</script>-->


</head>
<body>
<h2>Add user page</h2>
<a href="/">Return to start page</a>
<br/>
<a href="/report_users_form">Report list of users</a>
<br/>
<a href="/add_users_form">Add users (list of customers)</a>
<br/>

<hr/>
<form th:action="@{/users}" method="post">
    <div><label> First Name : <input type="text" name="firstName"/> </label></div>
    <br/>
    <div><label> Last Name : <input type="text" name="lastName"/> </label></div>
    <br/>
    <div><label> Password: <input type="password" name="password"/> </label></div>
    <br/>
    <div><label> Password confirm: <input type="password" name="password_confirm"/> </label></div>
<#--<input id="datetimepicker" name="dateOfPrice" value="2017-03-15" type="text"/>-->
<#--<input id="calendar" name="dateOfPrice" value="2017-03-15" type="text"/>-->
    <input type="text" id="datepicker" value="2017-03-15" name="dateOfPrice"/>
    <br/>
    <div><input type="submit" value="Add user"/></div>
    <!--<button type="submit">Submit</button>-->
</form>

</body>



<#--<script src="./src/main/webapp/resources/js/jquery.js"></script>-->
<#--&lt;#&ndash;<script src="\src\main\webapp\resources\js\jquery.js"></script>&ndash;&gt;-->
<#--<!--<script src="/resources/js/jquery.js"></script>&ndash;&gt;-->
<#--<script src="./src/main/webapp/resources/js/jquery.datetimepicker.full.js"></script>-->
<#--&lt;#&ndash;<script src="\src\main\webapp\resources\js\jquery.datetimepicker.full.js"></script>&ndash;&gt;-->
<#--<!--<script src="/resources/js/jquery.datetimepicker.full.js"></script>&ndash;&gt;-->
<#--<script>-->

    <#--$.datetimepicker.setLocale('en');-->

    <#--$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});-->
    <#--console.log($('#datetimepicker_format').datetimepicker('getValue'));-->

    <#--$("#datetimepicker_format_change").on("click", function(e){-->
        <#--$("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});-->
    <#--});-->
    <#--$("#datetimepicker_format_locale").on("change", function(e){-->
        <#--$.datetimepicker.setLocale($(e.currentTarget).val());-->
    <#--});-->

    <#--$('#datetimepicker').datetimepicker({-->
        <#--dayOfWeekStart : 1,-->
        <#--lang:'en',-->
        <#--disabledDates:['1986/01/08','1986/01/09','1986/01/10'],-->
        <#--startDate:	'1986/01/05'-->
    <#--});-->
    <#--$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});-->

    <#--$('.some_class').datetimepicker();-->

    <#--$('#default_datetimepicker').datetimepicker({-->
        <#--formatTime:'H:i',-->
        <#--formatDate:'d.m.Y',-->
        <#--//defaultDate:'8.12.1986', // it's my birthday-->
        <#--defaultDate:'+03.01.1970', // it's my birthday-->
        <#--defaultTime:'10:00',-->
        <#--timepickerScrollbar:false-->
    <#--});-->
    <#--$('#datetimepicker2').datetimepicker({-->
        <#--value:'2017-02-25',-->
        <#--yearOffset:222,-->
        <#--lang:'en',-->
        <#--timepicker:false,-->
        <#--format:'Y-m-d',-->
        <#--formatDate:'Y-m-d',-->
        <#--defaultDate:'2017-01-30',-->
        <#--//            minDate:'-1970/01/02', // yesterday is minimum date-->
        <#--//            maxDate:'+1970/01/02' // and tommorow is maximum date calendar-->
    <#--});-->

<#--</script>-->

</html>