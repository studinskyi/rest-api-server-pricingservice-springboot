-- use priceproducts;
-- DROP TABLE if EXISTS Product;
-- DROP TABLE if EXISTS Person;
insert into Product (id, name, timestamp, price) values (1, 'Bread', '2016-01-01T00:00:00', 15);
insert into Product (id, name, timestamp, price) values (2, 'Bread', '2016-01-03T00:00:00', 14);
insert into Product (id, name, timestamp, price) values (3, 'Bread', '2016-01-05T00:00:00', 18);
insert into Product (id, name, timestamp, price) values (4, 'Bananas', '2016-01-01T00:00:00', 21);
insert into Product (id, name, timestamp, price) values (5, 'Bananas', '2016-01-03T00:00:00', 23);
insert into Product (id, name, timestamp, price) values (6, 'Bananas', '2016-01-05T00:00:00', 27);
insert into Product (id, name, timestamp, price) values (7, 'Milk', '2016-01-01T00:00:00', 44);
insert into Product (id, name, timestamp, price) values (8, 'Milk', '2016-01-03T00:00:00', 43);
insert into Product (id, name, timestamp, price) values (9, 'Milk', '2016-01-05T00:00:00', 41);

insert into Person (id, first_name, last_name, password) values (1, 'Alexandr', 'Ivanov', '111');
insert into Person (id, first_name, last_name, password) values (2, 'Nikolay', 'Sidorov', '222');
insert into Person (id, first_name, last_name, password) values (3, 'Petr', 'Jonson', '333');

