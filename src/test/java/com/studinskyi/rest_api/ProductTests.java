package com.studinskyi.rest_api;

import com.studinskyi.rest_api.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductRepository productRepository;

    @Before
    public void deleteAllBeforeTests() throws Exception {
        productRepository.deleteAll();
    }

//    @Test
//    public void shouldReturnRepositoryIndex() throws Exception {
//
//        mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(
//                jsonPath("$._links.product").exists());
//    }

    @Test
    public void shouldCreateEntity() throws Exception {
        //insert into Product (id, name, timestamp, price) values (1, 'Bread', '2016-01-01T00:00:00', 15);
        mockMvc.perform(post("/product").content(
                "{\"name\": \"Bread\", \"timestamp\":\"2016-01-01T00:00:00\", \"price\":\"15\"}")).andExpect(
                status().isCreated()).andExpect(
                header().string("Location", containsString("product/")));
    }
//
//    @Test
//    public void shouldRetrieveEntity() throws Exception {
//
//        MvcResult mvcResult = mockMvc.perform(post("/product").content(
//                "{\"name\": \"Bread\", \"timestamp\":\"2016-01-01T00:00:00\", \"price\":\"15\"}")).andExpect(
//                //"{\"name\": \"Bread\", \"timestamp\":\"2016-01-01T00:00:00.000+0000\", \"price\":\"15\"}")).andExpect(
//                status().isCreated()).andReturn();
//
//        String location = mvcResult.getResponse().getHeader("Location");
//        mockMvc.perform(get(location)).andExpect(status().isOk())
//                .andExpect(jsonPath("$.name").value("Bread"))
//                .andExpect(jsonPath("$.timestamp").value("2016-01-01T00:00:00.000+0000"))
//                .andExpect(jsonPath("$.price").value("15"));
//    }


}
