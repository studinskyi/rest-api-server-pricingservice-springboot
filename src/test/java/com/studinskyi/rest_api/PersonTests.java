package com.studinskyi.rest_api;

import com.studinskyi.rest_api.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PersonTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    @Before
    public void deleteAllBeforeTests() throws Exception {
        personRepository.deleteAll();
    }

//    @Test
//    public void shouldReturnRepositoryIndex() throws Exception {
//
//        mockMvc.perform(get("/")).andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$._links.person").exists());
//    }

    @Test
    public void shouldCreateEntity() throws Exception {

        mockMvc.perform(post("/person").content(
                "{\"firstName\": \"Alexandr\", \"lastName\":\"Sidorov\", \"password\":\"111\"}"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", containsString("person/")));
    }
//
//    @Test
//    public void shouldRetrieveEntity() throws Exception {
//
//        MvcResult mvcResult = mockMvc.perform(post("/person").content(
//                "{\"firstName\": \"Alexandr\", \"lastName\":\"Sidorov\", \"password\":\"111\"}"))
//                .andExpect(status().isCreated()).andReturn();
//
//        String location = mvcResult.getResponse().getHeader("Location");
//        mockMvc.perform(get(location)).andExpect(status().isOk())
//                .andExpect(jsonPath("$.firstName").value("Alexandr"))
//                .andExpect(jsonPath("$.lastName").value("Sidorov"));
//                //.andExpect(jsonPath("$.password").value("111"));
//    }
//
//    @Test
//    public void shouldQueryEntity() throws Exception {
//
//        mockMvc.perform(post("/person").content(
//                "{\"firstName\": \"Alexandr\", \"lastName\":\"Sidorov\"}"))
//                .andExpect(status().isCreated());
//
//
//        mockMvc.perform(
//                get("/person/search/findByLastName?value={name}", "Sidorov"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$._embedded.person[0].firstName").value("Alexandr"));
//    }
//
//    @Test
//    public void shouldUpdateEntity() throws Exception {
//
//        MvcResult mvcResult = mockMvc.perform(post("/person").content(
//                "{\"firstName\": \"Alexandr\", \"lastName\":\"Sidorov\"}")).andExpect(
//                status().isCreated()).andReturn();
//
//        String location = mvcResult.getResponse().getHeader("Location");
//
//        mockMvc.perform(put(location).content(
//                "{\"firstName\": \"Oleg\", \"lastName\":\"Sidorov\"}")).andExpect(
//                status().isNoContent());
//
//        mockMvc.perform(get(location)).andExpect(status().isOk()).andExpect(
//                jsonPath("$.firstName").value("Oleg")).andExpect(
//                jsonPath("$.lastName").value("Sidorov"));
//    }
//
//    @Test
//    public void shouldPartiallyUpdateEntity() throws Exception {
//
//        MvcResult mvcResult = mockMvc.perform(post("/person").content(
//                "{\"firstName\": \"Alexandr\", \"lastName\":\"Sidorov\"}")).andExpect(
//                status().isCreated()).andReturn();
//
//        String location = mvcResult.getResponse().getHeader("Location");
//
//        mockMvc.perform(
//                patch(location).content("{\"firstName\": \"Roman\"}")).andExpect(
//                status().isNoContent());
//
//        mockMvc.perform(get(location)).andExpect(status().isOk()).andExpect(
//                jsonPath("$.firstName").value("Roman")).andExpect(
//                jsonPath("$.lastName").value("Sidorov"));
//    }
//
//    @Test
//    public void shouldDeleteEntity() throws Exception {
//
//        MvcResult mvcResult = mockMvc.perform(post("/person").content(
//                "{ \"firstName\": \"Alexandr\", \"lastName\":\"Sidorov\"}")).andExpect(
//                status().isCreated()).andReturn();
//
//        String location = mvcResult.getResponse().getHeader("Location");
//        System.out.println("location = " + location);
//        mockMvc.perform(delete(location)).andExpect(status().isNoContent());
//
//        mockMvc.perform(get(location)).andExpect(status().isNotFound());
//    }
}